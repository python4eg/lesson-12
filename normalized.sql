create table if not exists music(
    track_name text,
    album_name text,
    genre text,
    media_type text,
    composer text,
    composer_address text,
    artist text,
    artist_address text,
    album_price float
);
insert into music values
 ('Song 1', 'Album 1', 'rap', 'mp3', 'Johann Sebastian Bach', 'Україна, Рівне, вул. Чорновола, 14/16', 'Johann Sebastian Bach', 'Україна, Рівне, вул. Чорновола, 14/16', 99.99),
 ('Song 2', 'Album 1', 'rap', 'mp3', 'Johann Sebastian Bach', 'Україна, Рівне, вул. Чорновола, 14/16', 'Johann Sebastian Bach', 'Україна, Рівне, вул. Чорновола, 14/16', 99.99),
 ('Song 3', 'Album 1', 'rap', 'mp3', 'Johann Sebastian Bach', 'Україна, Рівне, вул. Чорновола, 14/16', 'Johann Sebastian Bach', 'Україна, Рівне, вул. Чорновола, 14/16', 99.99),
 ('Song 4', 'Album 1', 'rap', 'mp3', 'Johann Sebastian Bach', 'Україна, Рівне, вул. Чорновола, 14/16', 'Johann Sebastian Bach', 'Україна, Рівне, вул. Чорновола, 14/16', 99.99),
 ('Song 5', 'Album 1', 'rap', 'mp3', 'Johann Sebastian Bach', 'Україна, Рівне, вул. Чорновола, 14/16', 'Johann Sebastian Bach', 'Україна, Рівне, вул. Чорновола, 14/16', 99.99);

create table if not exists genre(id integer primary key autoincrement,
name text not null
);

create table if not exists media_type(id integer primary key autoincrement,
name text not null default 'N/A'
);

create table if not exists album(id integer primary key autoincrement,
    name text not null,
    price float not null
);

create table if not exists country(id integer primary key autoincrement,
    name text not null
);

create table if not exists city(id integer primary key autoincrement,
    name text not null
);

create table if not exists street(id integer primary key autoincrement,
    name text not null
);

create table if not exists address(id integer primary key autoincrement,
     country_id references country(id) on update cascade on delete cascade,
     city_id references city(id) on update cascade on delete cascade,
     street_id references street(id) on update cascade on delete cascade,
     build_number integer not null,
     apt integer
);

create table if not exists person(id integer primary key autoincrement,
    first_name text not null,
    mid_name text not null,
    last_name text not null,
    address_id references address(id) on update cascade on delete set null
);

create table if not exists music_normalized(id integer primary key autoincrement,
    track_name text not null,
    album_id references album(id) on update cascade on delete set null,
    genre_id references genre(id) on update cascade on delete set null,
    media_type_id references media_type(id) on update cascade on delete set null,
    composer_id references person(id) on update cascade on delete cascade,
    artist_id references person(id) on update cascade on delete cascade
);

insert into genre(name) values ('rap');
insert into media_type(name) values ('mp3');
insert into album(name, price) values ('Album 1', 99.99);
insert into country(name) values ('Ukraine');
insert into city(name) values ('Rivne');
insert into street(name) values ('Chornovola');
insert into address(country_id, city_id, street_id, build_number, apt) values (1, 1, 1, 14, 16);
insert into person(first_name, mid_name, last_name, address_id) values ( 'Johann', 'Sebastian', 'Bach', 1);
insert into music_normalized(track_name, album_id, genre_id, media_type_id, composer_id, artist_id) values
                                 ('Song 1', 1, 1, 1, 1, 1),
                                 ('Song 2', 1, 1, 1, 1, 1),
                                 ('Song 3', 1, 1, 1, 1, 1),
                                 ('Song 4', 1, 1, 1, 1, 1),
                                 ('Song 5', 1, 1, 1, 1, 1),
                                 ('Song 6', 1, 1, 1, 1, 1);
update album set price = 98.99 where name = 'Album 1';

select music_normalized.track_name, a.name, g.name, mt.name, p.first_name || ' ' || p.last_name, c1.name || ', ' || c2.name || ', ' || s.name || ', ' || adr.build_number || '/' || adr.apt
from music_normalized
join album a on music_normalized.album_id = a.id
join genre g on music_normalized.genre_id = g.id
join media_type mt on music_normalized.media_type_id = mt.id
join person p on music_normalized.artist_id = p.id

join address adr on p.address_id = adr.id
join country c1 on adr.country_id = c1.id
join city c2 on adr.city_id = c2.id
join street s on adr.street_id = s.id;
-- ('Song 1', 'Album 1', 'печерна пісня', 'mp3', 'Johann Sebastian Bach', 'Україна, Рівне, вул. Чорновола, 14/16', 'Johann Sebastian Bach', 'Україна, Рівне, вул. Чорновола, 14/16', 99.99),
-- ('Song 2', 'Album 1', 'rap', 'mp3', 'Johann Sebastian Bach', 'Україна, Рівне, вул. Чорновола, 14/16', 'Johann Sebastian Bach', 'Україна, Рівне, вул. Чорновола, 14/16', 99.99),
-- ('Song 3', 'Album 1', 'rap', 'mp3', 'Johann Sebastian Bach', 'Україна, Рівне, вул. Чорновола, 14/16', 'Johann Sebastian Bach', 'Україна, Рівне, вул. Чорновола, 14/16', 99.99),
-- ('Song 4', 'Album 1', 'rap', 'mp3', 'Johann Sebastian Bach', 'Україна, Рівне, вул. Чорновола, 14/16', 'Johann Sebastian Bach', 'Україна, Рівне, вул. Чорновола, 14/16', 99.99),
-- ('Song 5', 'Album 1', 'rap', 'mp3', 'Johann Sebastian Bach', 'Україна, Рівне, вул. Чорновола, 14/16', 'Johann Sebastian Bach', 'Україна, Рівне, вул. Чорновола, 14/16', 99.99);
update music set album_price = 98.99 where album_name = 'Album 1';