-- Test db
select * from tracks;

select distinct Composer from tracks;

select sum(Milliseconds) / 1000 / 3600 / 24 as days_to_listen_to from tracks;

select count(distinct Composer), count(Composer) from tracks where Milliseconds > 200000;

select * from genres;

select group_concat(Name, ' | ') from genres;

select min(Milliseconds), Name from tracks;
select max(Milliseconds), Name from tracks;

select Composer,
       count(Name) as tracks
from tracks
group by Composer
order by tracks desc;

select Composer,
       sum(Milliseconds) / 1000 as time
from tracks
group by Composer
order by time desc;


select Composer,
       count(Name) as tracks
from tracks
where Milliseconds > 300000
group by Composer
having tracks > 1
order by tracks desc
limit 10
offset 10;


select genres.Name,
       media_types.Name,
       count(tracks.Name) as tracks
from tracks
join Genres on tracks.GenreId = genres.GenreId
join media_types on tracks.MediaTypeId = media_types.MediaTypeId
where Milliseconds > 300000
group by genres.GenreId, media_types.MediaTypeId
having tracks between 2 and 5
order by tracks desc, genres.GenreId
limit 48
offset 0;
