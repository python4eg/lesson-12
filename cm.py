from collections import defaultdict


class CM:
    def __init__(self, name):
        self.__name = name
    def __enter__(self):
        self.obj = open(self.__name)
        return self.obj
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.obj.close()
        if exc_type is ValueError:
            print(exc_val)
            return True



with CM('1') as f:
    raise ValueError('Message')


print('Success')

#Data
tracks = [{'id': 1, 'name': '2'}, {'id': 1, 'name': '3'}, {'id': 1, 'name': '5'}, {'id': 2, 'name': '333'}, {'id': 3, 'name': None}]
print(tracks)

#Where
tracks = [i for i in tracks if i['name'] is not None]
print(tracks)
unique = {}
sum_u = {}
#Group by
for i in tracks:
    if i['id'] not in unique:
        unique[i['id']] = []
    unique[i['id']].append(i)
print(unique)

#count
for i in unique:
    sum_u[i] = len(unique[i])
print(sum_u)

#having
filter_sum_u = {}
for i in sum_u:
    if sum_u[i] > 1:
        filter_sum_u[i] = sum_u[i]
print(filter_sum_u)

